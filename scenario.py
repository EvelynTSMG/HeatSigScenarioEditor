from dataclasses import dataclass, field
from PyQt5.QtGui import QColor
from typing import Any

@dataclass
class CharAttributes:
	money: int = 0
	bleed: int = 0
	ammo0: int = 0
	ammo1: int = 0

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class CharStats:
	kills: int = 0
	knockouts: int = 0
	injuries: int = 0
	alarms: int = 0
	witnesses: int = 0
	missions_completed: int = 0
	missions_taken: int = 0
	clauses_completed: int = 0
	clauses_taken: int = 0
	avg_pay: int = 0

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class Item:
	kind: str = ''
	name: str = ''
	uses: int = 0
	timesUsed: int = 0
	value: int = 0
	rarity: int = 0

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class Character:
	forename: str = ''
	surname: str = ''
	head: str = 'Head_HelmetA_1'
	body: str = 'Female_SkinA_1'
	traits: list[int] = field(default_factory=list)
	attributes: CharAttributes = CharAttributes()
	stats: CharStats = CharStats()
	inventory: list[Item] = field(default_factory=list)

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class Pod:
	state: int = 0
	max_fuel: int = 0
	special: int = 0
	skin: int = 1
	thrust_color: QColor = QColor(0)

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class Mission:
	kind: str = 'Assassination'
	faction: int = 0
	target: str = ''
	difficulty: int = 0
	pay: int = 0
	bonus: int = 0
	desc: str = ''

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class GuardKit:
	role: str  = 'A'
	eq: list[str] = field(default_factory=list)

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value != None:
			super().__setattr__(__name, __value)

@dataclass
class Ship:
	desired_segments: int = 0
	sentries_per_segment: float = 0
	min_guards: int = 0
	guard_count: int = 0
	boss_count: int = 0
	keycard: str = 'Boss'
	random_guard_kit: bool = False
	random_boss_kit: bool = False
	life_link: bool = False
	alarm_type: str = 'No alarm'
	alarm_time: int = 0
	time_limit: int = 0
	autopilot: bool = False
	warzone: bool = False
	heat_sensors: bool = False
	inv_lock: bool = False
	jammer_gates: bool = False
	glitchproof: bool = False
	toughened: bool = False
	guard_kits: list[GuardKit] = field(default_factory=list)

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value is not None:
			super().__setattr__(__name, __value)


@dataclass
class Scenario:
	char: Character = Character()
	pod: Pod = Pod()
	mission: Mission = Mission()
	ship: Ship = Ship()

	def __setattr__(self, __name: str, __value: Any) -> None:
		if __value is not None:
			super().__setattr__(__name, __value)