import base64
import json
import sys
import os
from types import NoneType

from cv2 import split
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QFont, QIcon, QPixmap, QColor
from PyQt5.QtCore import QFile, QTextStream
from ScenarioEditorUI import Ui_MainWindow
import qdarkstyle
from appdirs import user_config_dir, user_data_dir
import utils.consts as consts
from utils.util import _finditempath, _writeAttrToFile, _writeHeaderToFile, _writeNL
from scenario import *
from typing import Any, Iterable, Optional, Union
from fastnumbers import isint
from datetime import datetime


class Window(QMainWindow, Ui_MainWindow):
	def __init__(self, parent=None):
		super().__init__(parent)
		self.app_name = 'HeatSigScenarioEditor'
		self.author = 'EvelynTSMG'
		self.last_opened_path = None
		self.last_dir = os.path.expanduser('~/Documents')
		self.pod_clr = QColor(0x0000FF) # Cheap as fuck
		self.setupUi(self)
		self.fixComboBoxes(self)
		self.connectSignalsSlots()
		self.loadConfig()
		self.reloadHeadCombos()
		self.reloadBodyCombos()
		if self.last_opened_path != None:
			self.loadScenario(self.last_opened_path)


	def fixComboBoxes(self, widget: QWidget) -> None:
		for wid in widget.children():
			if type(wid) is QComboBox:
				wid.setItemDelegate(QStyledItemDelegate(widget))
			elif len(wid.children()) > 0:
				self.fixComboBoxes(wid)


	def connectSignalsSlots(self) -> None:
		self.actionNew.triggered.connect(self.newScenario)
		self.actionOpen.triggered.connect(self.openScenario)
		self.actionSave.triggered.connect(self.saveScenario)
		self.actionSaveAs.triggered.connect(self.saveAsScenario)
		self.actionAbout.aboutToShow.connect(self.about) # A bit buggy, can't do anything about it

		self.btnTraitsAdd.clicked.connect(lambda: self.addItemToList(self.listTraits, self.inputTraits, self.btnTraitsAdd))
		self.btnTraitsRemove.clicked.connect(lambda: self.removeItemsFromList(self.listTraits, self.inputTraits, self.btnTraitsAdd, consts.TRAITS))
		self.btnTraitsClear.clicked.connect(lambda: self.clearItemsFromList(self.listTraits, self.inputTraits, self.btnTraitsAdd, consts.TRAITS))

		self.btnGuardAKitAdd.clicked.connect(lambda: self.addItemToList(self.listGuardAKit, self.inputGuardAKit, self.btnGuardAKitAdd))
		self.btnGuardAKitRemove.clicked.connect(lambda: self.removeItemsFromList(self.listGuardAKit, self.inputGuardAKit, self.btnGuardAKitAdd, consts.GUARD_KITS))
		self.btnGuardAKitClear.clicked.connect(lambda: self.clearItemsFromList(self.listGuardAKit, self.inputGuardAKit, self.btnGuardAKitAdd, consts.GUARD_KITS))

		self.btnGuardBKitAdd.clicked.connect(lambda: self.addItemToList(self.listGuardBKit, self.inputGuardBKit, self.btnGuardBKitAdd))
		self.btnGuardBKitRemove.clicked.connect(lambda: self.removeItemsFromList(self.listGuardBKit, self.inputGuardBKit, self.btnGuardBKitAdd, consts.GUARD_KITS))
		self.btnGuardBKitClear.clicked.connect(lambda: self.clearItemsFromList(self.listGuardBKit, self.inputGuardBKit, self.btnGuardBKitAdd, consts.GUARD_KITS))

		self.btnBossKitAdd.clicked.connect(lambda: self.addItemToList(self.listBossKit, self.inputBossKit, self.btnBossKitAdd))
		self.btnBossKitRemove.clicked.connect(lambda: self.removeItemsFromList(self.listBossKit, self.inputBossKit, self.btnBossKitAdd, consts.GUARD_KITS))
		self.btnBossKitClear.clicked.connect(lambda: self.clearItemsFromList(self.listBossKit, self.inputBossKit, self.btnBossKitAdd, consts.GUARD_KITS))

		self.inputHeadType.currentTextChanged.connect(self.reloadHeadCombos)
		self.inputBodyType.currentTextChanged.connect(self.reloadBodyCombos)

		self.inputPodType.currentTextChanged.connect(self.togglePodSkin)
		self.inputSpecifyGuardCount.stateChanged.connect(self.toggleGuardCount)
		self.inputAlarmType.currentTextChanged.connect(self.toggleAlarmTime)

		self.btnPodThrustColor.clicked.connect(self.changePodThrustColor)


	def sanitize_lines(self, lines: list[str]) -> list[str]:
		new_lines = lines.copy()
		for line in new_lines:
			line = line.replace('\r', '')
			line = line.replace('\n', '')
		return new_lines


	def decode_base64(self, lines: list[str]) -> list[str]:
		decoded_lines = []
		for line in lines:
			decoded_line = base64.b64decode(line).decode('utf-8')
			decoded_lines.append(decoded_line)
		return decoded_lines


	def newScenario(self) -> None:
		self.loadScenario(os.path.join('res', 'default_scenario.dat'))


	def loadScenario(self, filepath: Optional[str] = None) -> None:
		file = open(filepath)
		lines = file.read().splitlines()
		
		important_lines = [line for line in lines[5:] if line != '']
		is_encoded = lines[3].split(' = ')[1] == '1'
		if is_encoded:
			important_lines = self.decode_base64(important_lines)
		ilines = important_lines

		key = lambda line: line.split(' = ')[0] if len(line.split(' = ')) > 0 else None
		val = lambda line: line.split(' = ')[1] if len(line.split(' = ')) > 1 else None
		rgb2clr = lambda rgb: QColor(int(rgb[0]), int(rgb[1]), int(rgb[2]))

		scen = Scenario()

		headers = []
		new_header = {'name': '', 'items': {}}

		for line in ilines:
			if line[0] == '<' or line[:4] == 'Role': # is header
				if new_header['name'] != '':
					headers.append(new_header.copy())
					new_header = {'name': '', 'items': {}}
				if line[0] == '<':
					new_header['name'] = line[1:-1]
				elif line[:4] == 'Role':
					new_header['name'] = val(line)
			else:
				try:
					i = list(new_header['items'].keys()).index(key(line))
					if type(new_header['items'][key(line)]) is list:
						new_header['items'][key(line)].append(val(line))
					else:
						cur_value = new_header['items'][key(line)]
						new_header['items'][key(line)] = [cur_value, val(line)]
				except ValueError:
					new_header['items'][key(line)] = val(line)
		headers.append(new_header.copy())

		for header in headers:
			vals = header['items']
			match header['name']:
				case 'Character':
					scen.char.forename = vals.get('Forename', '')
					scen.char.surname = vals.get('Surname', '')
					scen.char.body = vals.get('Skin', '')
					scen.char.head = vals.get('Head', '')
					scen.char.attributes.bleed = int(vals.get('BleedOutTime', 0) or 0)
					scen.char.attributes.money = int(vals.get('Money', 0) or 0)
					scen.char.attributes.ammo0 = int(vals.get('Ammo[0]', 0) or 0)
					scen.char.attributes.ammo1 = int(vals.get('Ammo[1]', 0) or 0)
					scen.char.stats.kills = int(vals.get('Kills', 0) or 0)
					scen.char.stats.knockouts = int(vals.get('Knockouts', 0) or 0)
					scen.char.stats.injuries = int(vals.get('TimesInjured', 0) or 0)
					scen.char.stats.missions_completed = int(vals.get('MissionsCompleted', 0) or 0)
					scen.char.stats.missions_taken = int(vals.get('MissionsTaken', 0) or 0)
					scen.char.stats.clauses_completed = int(vals.get('ClausesCompleted', 0) or 0)
					scen.char.stats.clauses_taken = int(vals.get('ClausesTaken', 0) or 0)
					scen.char.stats.alarms = int(vals.get('Alarms', 0) or 0)
					scen.char.stats.witnesses = int(vals.get('LivingWitnesses', 0) or 0)
					scen.char.stats.avg_pay = int(vals.get('AveragePay', 0) or 0)
					if type(vals.get('Trait')) is list:
						for trait in vals['Trait']:
							scen.char.traits.append(trait)
					elif type(vals.get('Trait')) is not NoneType:
						scen.char.traits.append(vals['Trait'])
				case 'Pod':
					scen.pod.state = int(vals.get('PodState', 0) or 0)
					scen.pod.special = int(vals.get('SpecialPodType', 0) or 0)
					scen.pod.skin = int(vals.get('PodSkin', 6) if isint(vals.get('PodSkin', 6)) else 6)
					scen.pod.thrust_color = rgb2clr(vals.get('PodThrustColour', '  0,  0,  0').split(','))
					scen.pod.max_fuel = int(vals.get('PodMaxFuel', 100) if isint(vals.get('PodMaxFuel', 100)) else 100)
				case 'Item':
					new_item = Item()
					new_item.kind = vals.get('Type', 'oWrench')
					new_item.name = vals.get('Name', 'Wrench')
					new_item.rarity = int(vals.get('Rarity', 0))
					new_item.timesUsed = int(vals.get('TimesUsed', 0))
					new_item.uses = int(vals.get('Uses', 0))
					new_item.value = int(vals.get('Value', 0))
					scen.char.inventory.append(new_item)
				case 'Mission':
					scen.mission.kind = vals.get('Type', 'Assassination')
					scen.mission.faction = int(vals.get('FactionIndex', 0))
					scen.mission.pay = int(vals.get('Pay', 0))
					scen.mission.bonus = int(vals.get('Bonus', 0))
					scen.mission.target = vals.get('Target', '')
					scen.mission.desc = vals.get('Description', '')
					scen.mission.difficulty = int(vals.get('Difficulty', 0))
				case 'Ship':
					scen.ship.desired_segments = int(vals.get('DesiredSegmentCount', 1) if isint(vals.get('DesiredSegmentCount', 1)) else 1)
					scen.ship.sentries_per_segment = float(vals['SentriesPerSegment'] or 0)
					scen.ship.min_guards = int(vals.get('MinGuardsPerCluster', 3) if isint(vals.get('MinGuardsPerCluster', 3)) else 3)
					scen.ship.guard_count = int(vals.get('GuardCount', 0) or 0)
					scen.ship.boss_count = int(vals.get('BossCount', 0) or 0)
					scen.ship.keycard = vals.get('KeycardGuardRole', 'Boss')
					scen.ship.random_guard_kit = vals.get('RandomGuardKit', '0') == '1'
					scen.ship.random_boss_kit = vals.get('RandomBossKit', '0') == '1'
					scen.ship.life_link = vals.get('LifeLink', '0') == '1'
					if vals.get('EscapeProtocol', '0') == '1' and vals.get('ReinforcementProtocol', '0') == '1':
						scen.ship.alarm_type = 'Guarded Fleeing'
					elif vals.get('EscapeProtocol', '0') == '1':
						scen.ship.alarm_type = 'Target Flees'
					elif vals.get('ReinforcementProtocol', '0') == '1':
						scen.ship.alarm_type = 'Reinforcements'
					else:
						scen.ship.alarm_type = 'No Alarm'
					scen.ship.alarm_time = int(vals.get('AlarmTime', 0) or 0)
					scen.ship.time_limit = int(vals.get('TimeLimit', 0) or 0)
					scen.ship.autopilot = vals.get('Autopilot', '0') == '1'
					scen.ship.warzone = vals.get('Warzone', '0') == '1'
					scen.ship.heat_sensors = vals.get('Heat Sensors', '0') == '1'
					scen.ship.inv_lock = vals.get('HandLuggage', '0') == '1'
					scen.ship.jammer_gates = vals.get('ScramblerGates', '0') == '1'
					scen.ship.glitchproof = vals.get('GlitchProofHull', '0') == '1'
					scen.ship.toughened = vals.get('ToughenedHull', '0') == '1' # Doesn't fucking work
				case 'A': # Guard kits for role A
					scen.ship.guard_kits.append(GuardKit('A', vals.get('Kit', [])))
				case 'B': # Guard kits for role B
					scen.ship.guard_kits.append(GuardKit('B', vals.get('Kit', [])))
				case 'Boss': # Guard kits for Bosses
					scen.ship.guard_kits.append(GuardKit('Boss', vals.get('Kit', [])))

			# And now, we load into the app

		self.inputForename.setText(scen.char.forename)
		self.inputSurname.setText(scen.char.surname)
		foundBody = _finditempath(consts.BODIES, scen.char.body)
		body = foundBody.split('|')
		self.inputBodyType.setCurrentText(body[0])
		self.inputBodyID.setCurrentText(body[1])
		foundHead = _finditempath(consts.HEADS, scen.char.head)
		head = foundHead.split('|')
		self.inputHeadType.setCurrentText(head[0])
		self.inputHeadID.setCurrentText(head[1])
		self.inputBleed.setValue(scen.char.attributes.bleed)
		self.inputMoney.setValue(scen.char.attributes.money)
		self.inputAmmo0.setValue(scen.char.attributes.ammo0)
		self.inputAmmo1.setValue(scen.char.attributes.ammo1)
		self.inputKills.setValue(scen.char.stats.kills)
		self.inputKnockouts.setValue(scen.char.stats.knockouts)
		self.inputInjuries.setValue(scen.char.stats.injuries)
		self.inputMissionsCompleted.setValue(scen.char.stats.missions_completed)
		self.inputMissionsTaken.setValue(scen.char.stats.missions_taken)
		self.inputClausesCompleted.setValue(scen.char.stats.clauses_completed)
		self.inputClausesTaken.setValue(scen.char.stats.clauses_taken)
		self.inputAlarms.setValue(scen.char.stats.alarms)
		self.inputWitnesses.setValue(scen.char.stats.witnesses)
		self.inputAvgPay.setValue(scen.char.stats.avg_pay)
		self.clearItemsFromList(self.listTraits, self.inputTraits, self.btnTraitsAdd, consts.TRAITS)
		for trait in scen.char.traits:
			self.inputTraits.setCurrentText(_finditempath(consts.TRAITS, trait))
			self.addItemToList(self.listTraits, self.inputTraits, self.btnTraitsAdd)
		if  self.inputTraits.count() > 0:
			self.inputTraits.setCurrentIndex(0)

		self.inputPodState.setCurrentIndex(scen.pod.state)
		self.inputPodType.setCurrentIndex(scen.pod.special)
		self.inputPodSkin.setCurrentIndex(scen.pod.skin - 1)
		thrust_pixmap = QPixmap(26, 26)
		thrust_pixmap.fill(scen.pod.thrust_color)
		self.imgPodThrustColor.setPixmap(thrust_pixmap)
		self.inputPodFuel.setValue(scen.pod.max_fuel)

		#TODO: Item stuff
		# new_item
		# new_item.kind
		# new_item.name
		# new_item.rarity
		# new_item.timesUsed
		# new_item.uses
		# new_item.value
		self.inputType.setCurrentText(scen.mission.kind)
		self.inputFaction.setCurrentIndex(scen.mission.faction)
		self.inputPay.setValue(scen.mission.pay)
		self.inputBonus.setValue(scen.mission.bonus)
		self.inputTarget.setText(scen.mission.target)
		self.inputDesc.setPlainText(scen.mission.desc)
		self.inputDiff.setValue(scen.mission.difficulty)
		self.inputDesiredSegments.setValue(scen.ship.desired_segments)
		self.inputSentriesPerSegment.setValue(scen.ship.sentries_per_segment)
		self.inputMinGuards.setValue(scen.ship.min_guards)
		self.inputGuardCount.setValue(scen.ship.guard_count)
		self.inputBossCount.setValue(scen.ship.boss_count)
		if self.inputGuardCount.value() > 0 or self.inputBossCount.value() > 0:
			self.inputSpecifyGuardCount.setCheckState(2)
		self.inputKeycardRole.setCurrentText(scen.ship.keycard)
		self.inputRandomKitGuards.setCheckState(2 if scen.ship.random_guard_kit else 0)
		self.inputRandomKitBoss.setCheckState(2 if scen.ship.random_boss_kit else 0)
		self.inputLifeLink.setCheckState(2 if scen.ship.life_link else 0)
		self.inputAlarmType.setCurrentText(scen.ship.alarm_type)
		if self.inputAlarmType.currentText() == 'No Alarm':
			self.inputAlarmTime.setEnabled(False)
		self.inputAlarmTime.setValue(scen.ship.alarm_time)
		self.inputTimeLimit.setValue(scen.ship.time_limit)
		self.inputAutopilot.setCheckState(2 if scen.ship.autopilot else 0)
		self.inputWarzone.setCheckState(2 if scen.ship.autopilot else 0)
		self.inputHeatSensors.setCheckState(2 if scen.ship.heat_sensors else 0)
		self.inputInventoryLock.setCheckState(2 if scen.ship.inv_lock else 0)
		self.inputJammerGates.setCheckState(2 if scen.ship.inv_lock else 0)
		self.inputGlitchProof.setCheckState(2 if scen.ship.glitchproof else 0)
		self.inputToughened.setCheckState(2 if scen.ship.toughened else 0)
		self.clearItemsFromList(self.listGuardAKit, self.inputGuardAKit, self.btnGuardAKitAdd, consts.GUARD_KITS)
		self.clearItemsFromList(self.listGuardBKit, self.inputGuardBKit, self.btnGuardBKitAdd, consts.GUARD_KITS)
		self.clearItemsFromList(self.listBossKit, self.inputBossKit, self.btnBossKitAdd, consts.GUARD_KITS)
		for kit in scen.ship.guard_kits:
			for eq in kit.eq:
				match kit.role:
					case 'A':
						self.inputGuardAKit.setCurrentText(eq)
						self.addItemToList(self.listGuardAKit, self.inputGuardAKit, self.btnGuardAKitAdd)
					case 'B':
						self.inputGuardBKit.setCurrentText(eq)
						self.addItemToList(self.listGuardBKit, self.inputGuardBKit, self.btnGuardBKitAdd)
					case 'Boss':
						self.inputBossKit.setCurrentText(eq)
						self.addItemToList(self.listBossKit, self.inputBossKit, self.btnBossKitAdd)
		if self.inputGuardAKit.count() > 0:
			self.inputGuardAKit.setCurrentIndex(0)
		if self.inputGuardBKit.count() > 0:
			self.inputGuardBKit.setCurrentIndex(0)
		if self.inputBossKit.count() > 0:
			self.inputBossKit.setCurrentIndex(0)


	def openScenario(self) -> None:		
		ofd = QFileDialog(None, 'Open file', self.last_dir)
		ofd.setNameFilters(['All files (*.*)', 'Heat Signature scenario files (*.dat)'])
		ofd.selectNameFilter('Heat Signature scenario files (*.dat)')
		if ofd.exec():
			filepath = ofd.selectedFiles()[0]
			self.last_opened_path = filepath
			self.last_dir = os.path.split(filepath)[0]
			self.loadScenario(filepath)


	def saveScenario(self) -> None:
		if self.last_opened_path is not None:
			self.saveAsScenario(self.last_opened_path)
		else:
			self.saveAsScenario()


	def saveAsScenario(self, filepath: Optional[str] = None) -> None:
		if filepath is None:
			sfd = QFileDialog(None, 'Save file', self.last_dir)
			sfd.setNameFilters(['All files (*.*)', 'Heat Signature scenario files (*.dat)'])
			sfd.selectNameFilter('Heat Signature scenario files (*.dat)')
			if sfd.exec():
				filepath = sfd.selectedFiles()[0]
				self.last_opened_path = filepath
				self.last_dir = os.path.split(filepath)[0]
			else:
				return

		file = open(filepath, 'w')
		_writeHeaderToFile(file, 'Header')
		cur_time = datetime.utcnow()
		cur_time_f = cur_time.strftime('%Y-%-m-%-d %-H:%-M:%-S UTC')

		_writeAttrToFile(file, 'Time', cur_time_f)
		_writeAttrToFile(file, 'TimeNumber', cur_time.timestamp())
		_writeAttrToFile(file, 'Encoded', 0)
		_writeNL(file)
		_writeHeaderToFile(file, 'Character')
		_writeAttrToFile(file, 'Status', 'Available')
		_writeAttrToFile(file, 'Forename', self.inputForename.text())
		_writeAttrToFile(file, 'Surname', self.inputSurname.text())
		_writeAttrToFile(file, 'Skin', consts.BODIES[self.inputBodyType.currentText()][self.inputBodyID.currentText()])
		_writeAttrToFile(file, 'Head', consts.HEADS[self.inputHeadType.currentText()][self.inputHeadID.currentText()])
		_writeAttrToFile(file, 'BleedOutTime', self.inputBleed.value())
		_writeAttrToFile(file, 'Money', self.inputMoney.value())
		_writeAttrToFile(file, 'Ammo[0]', self.inputAmmo0.value())
		_writeAttrToFile(file, 'Ammo[1]', self.inputAmmo1.value())
		# _writeAttrToFile(file, 'Accolades', '')
		# _writeAttrToFile(file, 'PersonalMissionCost', 0)
		# _writeAttrToFile(file, 'PersonalMissionRescueAgent', '')
		# _writeAttrToFile(file, 'PersonalMissionState', 0)
		_writeAttrToFile(file, 'Kills', self.inputKills.value())
		_writeAttrToFile(file, 'Knockouts', self.inputKnockouts.value())
		_writeAttrToFile(file, 'TimesInjured', self.inputInjuries.value())
		_writeAttrToFile(file, 'MissionsCompleted', self.inputMissionsCompleted.value())
		_writeAttrToFile(file, 'MissionsTaken', self.inputMissionsTaken.value())
		_writeAttrToFile(file, 'ClausesCompleted', self.inputClausesCompleted.value())
		_writeAttrToFile(file, 'ClausesTaken', self.inputClausesTaken.value())
		_writeAttrToFile(file, 'Alarms', self.inputAlarms.value())
		_writeAttrToFile(file, 'LivingWitnesses', self.inputWitnesses.value())
		_writeAttrToFile(file, 'AveragePay', self.inputAvgPay.value())

		_writeHeaderToFile(file, 'Pod')
		_writeAttrToFile(file, 'PodState', self.inputPodState.currentIndex())
		_writeAttrToFile(file, 'SpecialPodType', self.inputPodType.currentIndex())
		if self.inputPodSkin.currentText() == 'Random':
			_writeAttrToFile(file, 'PodSkin', '')
		else:
			_writeAttrToFile(file, 'PodSkin', self.inputPodSkin.currentText())
		pad_r = str(self.pod_clr.red()).rjust(3, ' ')
		pad_g = str(self.pod_clr.green()).rjust(3, ' ')
		pad_b = str(self.pod_clr.blue()).rjust(3, ' ')
		_writeAttrToFile(file, 'PodThrustColor', f'{pad_r},{pad_g},{pad_b}')
		_writeAttrToFile(file, 'PodMaxFuel', self.inputPodFuel.value())

		for itemWidget in self.gridItems.children():
			print(itemWidget.objectName())

		_writeHeaderToFile(file, 'Mission')
		_writeAttrToFile(file, 'Type', self.inputType.currentText())
		_writeAttrToFile(file, 'Context', '')
		_writeAttrToFile(file, 'FactionIndex', self.inputFaction.currentIndex())
		_writeAttrToFile(file, 'Pay', self.inputPay.value())
		_writeAttrToFile(file, 'Bonus', self.inputBonus.value())
		_writeAttrToFile(file, 'Target', self.inputTarget.text())
		_writeAttrToFile(file, 'LovedOne', '')
		_writeAttrToFile(file, 'Description', self.inputDesc.plainText().replace('\n', ' '))
		_writeAttrToFile(file, 'Difficulty', self.inputDiff.value())

		_writeHeaderToFile(file, 'Ship')
		_writeAttrToFile(file, 'DesiredSegmentCount', self.inputDesiredSegments.value())
		_writeAttrToFile(file, 'SentriesPerSegment', self.inputSentriesPerSegment.value())
		_writeAttrToFile(file, 'MinGuardsPerCluster', self.inputMinGuards.value())
		_writeAttrToFile(file, 'GuardCount', self.inputGuardCount.value())
		_writeAttrToFile(file, 'BossCount', self.inputBossCount.value())
		_writeAttrToFile(file, 'KeycardGuardRole', self.inputKeycardRole.currentText())
		if self.inputRandomKitGuards.checkState() == 2:
			_writeAttrToFile(file, 'RandomGuardKit', 1)
		if self.inputRandomKitBoss.checkState() == 2:
			_writeAttrToFile(file, 'RandomBossKit', 1)
		if self.inputLifeLink.checkState() == 2:
			_writeAttrToFile(file, 'LifeLink', 1)
		match self.inputAlarmType.currentText():
			case 'Target Flees':
				_writeAttrToFile(file, 'EscapeProtocol', 1)
			case 'Reinforcements':
				_writeAttrToFile(file, 'ReinforcementProtocol', 1)
			case 'Guarded Fleeing':
				_writeAttrToFile(file, 'EscapeProtocol', 1)
				_writeAttrToFile(file, 'ReinforcementProtocol', 1)
		if self.inputAlarmType.currentText() != 'No Alarm' and self.inputAlarmTime.value() != 0:
			_writeAttrToFile(file, 'AlarmTime', self.inputAlarmTime.value())
		else:
			_writeAttrToFile(file, 'AlarmTime', -1)
		if self.inputTimeLimit.value() != 0:
			_writeAttrToFile(file, 'TimeLimit', self.inputTimeLimit.value())
		else:
			_writeAttrToFile(file, 'TimeLimit', -1)
		if self.inputAutopilot.checkState() == 2:
			_writeAttrToFile(file, 'Autopilot', 1)
		if self.inputWarzone.checkState() == 2:
			_writeAttrToFile(file, 'Warzone', 1)
		if self.inputHeatSensors.checkState() == 2:
			_writeAttrToFile(file, 'HeatSensor', 1)
		if self.inputInventoryLock.checkState() == 2:
			_writeAttrToFile(file, 'HandLuggage', 1)
		if self.inputJammerGates.checkState() == 2:
			_writeAttrToFile(file, 'ScramblerGates', 1)
		if self.inputGlitchProof.checkState() == 2:
			_writeAttrToFile(file, 'GlitchProofHull', 1)
		if self.inputToughened.checkState() == 2:
			_writeAttrToFile(file, 'ToughenedHull', 1) # We'll just pretend like this does anything




	def about(self):
		QMessageBox.about(None, 'About', 'Heat Signature Scenario Editor\n\nBy EvelynTSMG\nWith the support of her\nlovely girlfriend')


	def saveConfig(self):
		config_dir = user_config_dir(self.app_name, self.author)
		config_file = open(config_dir, 'w')
		config_file.write('last_opened='+self.last_opened_path)


	def loadConfig(self):
		config_dir = user_config_dir(self.app_name, self.author)
		if os.path.exists(config_dir):
			config_file = open(config_dir, 'r')
			last_opened = config_file.readline().split('=')
			if len(last_opened) > 1:
				last_opened_path = last_opened[1]
				self.openScenario(last_opened_path)


	def reloadHeadCombos(self):
		head_type = self.inputHeadType.currentText()
		head_ids = consts.HEADS[head_type]
		self.inputHeadID.clear()
		for head in head_ids:
			self.inputHeadID.addItem(head)


	def reloadBodyCombos(self):
		body_type = self.inputBodyType.currentText()
		body_ids = consts.BODIES[body_type]
		self.inputBodyID.clear()
		for body in body_ids:
			self.inputBodyID.addItem(body)


	def addItemToList(self, listWidget: QListWidget, inputWidget: QComboBox, addWidget: QToolButton) -> None:
		listWidget.addItem(inputWidget.currentText())
		inputWidget.removeItem(inputWidget.currentIndex())
		if inputWidget.count() <= 0:
			addWidget.setEnabled(False)
			

	def removeItemsFromList(self, listWidget: QListWidget, inputWidget: QComboBox, addWidget: QToolButton, defaults: Union[dict, Iterable]) -> None:
		last_item = inputWidget.currentText()
		for item in listWidget.selectedItems():
			row = listWidget.row(item)
			inputWidget.addItem(item.text())
			listWidget.takeItem(row)
			addWidget.setEnabled(True)
		if type(defaults) is dict:
			self.sortComboBox(inputWidget, list(defaults.keys()))
		elif type(defaults) is list:
			self.sortComboBox(inputWidget, defaults)
		inputWidget.setCurrentText(last_item)


	def clearItemsFromList(self, listWidget: QListWidget, inputWidget: QComboBox, addWidget: QToolButton, defaults: Union[dict, Iterable]) -> None:
		last_item = inputWidget.currentText()
		while listWidget.count() > 0:
			item = listWidget.item(0)
			inputWidget.addItem(item.text())
			listWidget.takeItem(0)
			addWidget.setEnabled(True)
		if type(defaults) is dict:
			self.sortComboBox(inputWidget, list(defaults.keys()))
		elif type(defaults) is list:
			self.sortComboBox(inputWidget, defaults)
		inputWidget.setCurrentText(last_item)


	def _takeItemFromComboBox(self, comboBox: QComboBox, idx: int) -> str:
		item = comboBox.itemText(idx)
		comboBox.removeItem(idx)
		return item


	def sortComboBox(self, comboBox: QComboBox, key: list) -> None:
		items = [self._takeItemFromComboBox(comboBox, 0) for i in range(comboBox.count())]
		items.sort(key=lambda item: key.index(item))
		comboBox.addItems(items)


	def toggleGuardCount(self, state):
		enable = True if state == 2 else False
		self.inputMinGuards.setEnabled(not enable)
		self.inputGuardCount.setEnabled(enable)
		self.inputBossCount.setEnabled(enable)


	def togglePodSkin(self, state):
		enable = True if state == 'Breacher Pod' else False
		self.inputPodSkin.setEnabled(enable)


	def toggleAlarmTime(self, state):
		enable = False if state == 'No Alarm' else True
		self.inputAlarmTime.setEnabled(enable)


	def changePodThrustColor(self):
		qcd = QColorDialog()


if __name__ == '__main__':
	app = QApplication(sys.argv)

	app.setStyleSheet(qdarkstyle.load_stylesheet(palette=qdarkstyle.DarkPalette))
	app.setFont(QFont('Xolonium', 12))

	win = Window()
	win.show()
	sys.exit(app.exec())