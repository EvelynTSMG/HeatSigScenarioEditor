TRAITS = {
	'Weak': 1,
	'Technophobe': 2,
	'Glitch Sickness': 3,
	'Proud': 4,
	'Shaky': 5,
	'Frail': 6,
	'Dying': 7,
	'Informed': 8,
	'Unambitious': 9,
	'Tough': 10,
	'Rich': 11,
	'Heir': 12,
	'Supplier': 13,
	'Vindictive': 14,
	'Silencer': 15,
	'Ex-Glitcher': 16,
	'Ex-Sovereign': 17,
	'Ex-Foundry': 18,
	'Ex-Offworld': 19,
	'Bloodless Vow': 20,
	'Pacifist Vow': 21,
	'Ghost Vow': 22,
	'Blacklisted': 23,
	'Lucky': 24,
#	'0': 25,
	'Failed Bloodless Vow': 26,
	'Failed Pacifist Vow': 27,
	'Failed Ghost Vow': 28,
	'Liberator': 29,
	'Obsessed': 30,
	'No Pockets': 31,
}

GUARD_KITS = [
	'Gun',
	'Shotgun',
	'Concussive Gun',
	'Concussive Shotgun',
	'Soft Shield',
	'Emergency Shield',
	'Shield',
	'Armour',
	'Explosive',
	'Glitch Dash',
	'Heat Sensor',
]

HEADS = {
	'Helmet': {
		'A1': 'Head_HelmetA_1', 'A2': 'Head_HelmetA_2', 'A3': 'Head_HelmetA_3', 'A4': 'Head_HelmetA_4',
		'B1': 'Head_HelmetB_1', 'B2': 'Head_HelmetB_2', 'B3': 'Head_HelmetB_3', 'B4': 'Head_HelmetB_4',
		'C1': 'Head_HelmetC_1', 'C2': 'Head_HelmetC_2', 'C3': 'Head_HelmetC_3', 'C4': 'Head_HelmetC_4',
		'D1': 'Head_HelmetD_1', 'D2': 'Head_HelmetD_2', 'D3': 'Head_HelmetD_3', 'D4': 'Head_HelmetD_4',
	},
	'Female Hair': {
		'A1': 'HeadFemale_HairA_1', 'A2': 'HeadFemale_HairA_2', 'A3': 'HeadFemale_HairA_3', 'A4': 'HeadFemale_HairA_4',
		'B1': 'HeadFemale_HairB_1', 'B2': 'HeadFemale_HairB_2', 'B3': 'HeadFemale_HairB_3', 'B4': 'HeadFemale_HairB_4',
		'C1': 'HeadFemale_HairC_1', 'C2': 'HeadFemale_HairC_2', 'C3': 'HeadFemale_HairC_3', 'C4': 'HeadFemale_HairC_4',
		'D1': 'HeadFemale_HairD_1', 'D2': 'HeadFemale_HairD_2', 'D3': 'HeadFemale_HairD_3', 'D4': 'HeadFemale_HairD_4',
	},
	'Male Hair': {
		'A1': 'HeadMale_HairA_1', 'A2': 'HeadMale_HairA_2', 'A3': 'HeadMale_HairA_3', 'A4': 'HeadMale_HairA_4',
		'B1': 'HeadMale_HairB_1', 'B2': 'HeadMale_HairB_2', 'B3': 'HeadMale_HairB_3', 'B4': 'HeadMale_HairB_4',
		'C1': 'HeadMale_HairC_1', 'C2': 'HeadMale_HairC_2', 'C3': 'HeadMale_HairC_3', 'C4': 'HeadMale_HairC_4',
		'D1': 'HeadMale_HairD_1', 'D2': 'HeadMale_HairD_2', 'D3': 'HeadMale_HairD_3', 'D4': 'HeadMale_HairD_4',
		'E1': 'HeadMale_HairE_1',
	},
	'Contractor': {
		'Tracker': 'Head_Helmet_F1',
		'Defender': 'Head_Helmet_F2',
		'Jammer': 'Head_Helmet_F3',
		'Predator': 'Head_Helmet_F4',
	}
}

BODIES = {
	'Female Body': {
		'A1': 'Female_SkinA_1', 'A2': 'Female_SkinA_2', 'A3': 'Female_SkinA_3', 'A4': 'Female_SkinA_4',
		'B1': 'Female_SkinB_1', 'B2': 'Female_SkinB_2', 'B3': 'Female_SkinB_3', 'B4': 'Female_SkinB_4',
		'C1': 'Female_SkinC_1', 'C2': 'Female_SkinC_2', 'C3': 'Female_SkinC_3', 'C4': 'Female_SkinC_4',
		'D1': 'Female_SkinD_1', 'D2': 'Female_SkinD_2', 'D3': 'Female_SkinD_3', 'D4': 'Female_SkinD_4',
	},
	'Male Body': {
		'A1': 'Male_SkinA_1', 'A2': 'Male_SkinA_2', 'A3': 'Male_SkinA_3', 'A4': 'Male_SkinA_4',
		'B1': 'Male_SkinB_1', 'B2': 'Male_SkinB_2', 'B3': 'Male_SkinB_3', 'B4': 'Male_SkinB_4',
		'C1': 'Male_SkinC_1', 'C2': 'Male_SkinC_2', 'C3': 'Male_SkinC_3', 'C4': 'Male_SkinC_4',
		'D1': 'Male_SkinD_1', 'D2': 'Male_SkinD_2', 'D3': 'Male_SkinD_3', 'D4': 'Male_SkinD_4',
		'E1': 'Male_SkinE_1',
	},
	'Contractor': {
		'Tracker': 'Male_Skin_F1',
		'Defender': 'Male_Skin_F2',
		'Jammer': 'Male_Skin_F3',
		'Predator': 'Male_Skin_F4',
	}
}

GUN_SPRITES = {
	'Default': '',

	# Pistols
	'Pistol': 'sPistol',
	'Standard Pistol': 'sPistolStandard',
	'Green Pistol': 'sGreenPistol',
	'Red Pistol': 'sPistolRed',
	'Short Red Pistol': 'sPistolShortRed',
	'Snub Pistol': 'sSnubPistol',
	'Automatic Pistol': 'sAutomaticPistol',
	'Concussive Pistol': 'sConcussivePistol',
	'Heavy Concussive Pistol': 'sConcussivePistolHeavy',

	# Rifles
	'AssaultRifle': 'sAssaultRifle',
	'Slender Rifle': 'sSlenderRifle',
	'Boxy Rifle': 'sBoxyRifle',
	'Green Bullpup Rifle': 'sRifleBullpupGreen',
	'Automatic Rifle': 'sAutomaticRifle',
	'Concussive Rifle': 'sConcussiveRifle',
	'Piercing Rifle': 'sPiercingRifle',
	'Piercing Concussive Rifle': 'sConcussiveRiflePiercing',

	# Shotguns
	'Shotgun': 'sShotgun',
	'Shotgun With Strap': 'sShotgunStrap',
	'Concussive Shotgun': 'sConcussiveShotgun',
}