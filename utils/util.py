from typing import TextIO, Union

def _finditempath(obj: dict, item: str, path: str = ''):
	for k, v in obj.items():
		if str(v) == item:
			path += k
			return path
		elif type(v) == dict:
			path += k + '|'
			found = _finditempath(v, item, path)
			if found != None:
				return found
			path = path[:len(path) - len(k) - 1]
	return None


def _writeHeaderToFile(file: TextIO, header: str):
	file.write(f'<{header}>\n')

def _writeAttrToFile(file: TextIO, name: str, value: Union[str, int, float]):
	if type(value) is not str:
		value = str(value)
	file.write(f'{name} = {value}\n')

def _writeNL(file: TextIO):
	file.write('\n')